global find_word
extern string_equals

section .text

find_word:
    cmp rsi, 0
    je .fail
    add rsi, 8 
    call string_equals
    test rax, rax 
    jnz .extractAddress 
    sub rsi, 8
    mov rsi, [rsi] 
    jmp find_word

.fail:
    xor rax, rax 
    ret

.extractAddress: 
    sub rsi, 8
    mov rax, rsi
    ret

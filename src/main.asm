%include "words.inc"

global _start
extern print_string
extern print_err_string
extern print_newline
extern string_length
extern exit
extern find_word
extern read_word

%define buffer_size 256

section .rodata
welcome: db "Введите ключ:", 10, 0
found: db "Совпадение найдено; значение: ", 0
not_found: db "Ошибка: Совпадение не найдено", 10, 0
out_of_buffer: db "Ошибка: Введеный ключ длинее 256 символов", 10, 0
nl_line: db 10
section .text
_start:
    mov rdi, welcome
    call print_string

    sub rsp, buffer_size 
    mov rsi, buffer_size
    mov rdi, rsp
    call read_word 
    cmp rdx, buffer_size
    jg .read_out_of 

    mov rsi, entry 
    mov rdi, rax
    call find_word
    test rax, rax
    jz .fail 

    push rax
    mov rdi, found
    call print_string
    pop rax

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1
    call print_string
    mov rdi, nl_line
    call print_string
    add rsp, buffer_size
call exit

.read_out_of:
    mov rdi, out_of_buffer
    call print_err_string
    mov rdi, 1
    call exit
ret

.fail:
    mov rdi, not_found
    call print_err_string
    mov rdi, 1
    call exit
ret

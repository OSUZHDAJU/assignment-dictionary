%define entry 0
%macro colon 2
    %%next: dq entry
    db %1, 0
    %2:
%define entry %%next
%endmacro

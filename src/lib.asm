global exit
global string_length
global print_string
global print_err_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_char_and_join_spaces
global read_word
global parse_uint
global parse_int
global string_copy

%define stdout 1
%define stderr 2

section .data
nl_char: db 10 
section .text

; Принимает код возврата и завершает текущий процесс
; argumet - rdi
exit:
    mov rax, 60 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; ardument - rdi
string_length:
    xor rax, rax 
    .loop:
          cmp byte [rdi+rax], 0 
          je .end 
          inc rax 
          jmp .loop 
    .end:
          ret
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; ardument - rdi
print_err_string:
    call string_length  
    mov rsi, rdi 
    mov rdi, stderr 
    jmp print_current_string
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; ardument - rdi
print_string:
    call string_length 
    mov rsi, rdi 
    mov rdi, stdout

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; ardument - rdi
print_current_string: 
    mov rdx, rax 
    mov rax, 1 
    syscall
    ret

; Принимает код символа и выводит его в stdout
; argument - rdi
print_char:
    push rdi 
    mov rax, 1 
    mov rsi, rsp 
    mov rdi, 1 
    mov rdx, 1 
    syscall
    pop rdi 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, nl_char 
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp 
    push rbx

    mov rbx, 10 
    mov rax, rdi 
    mov rbp, rsp 
    dec rsp
    .divide:
      xor rdx, rdx 
      div rbx 
      add dl, '0' 
      dec rsp
      mov [rsp], dl 
      test ax, ax 
    jnz .divide

    mov rdi, rsp 
    call print_string
    mov rsp, rbp 

    pop rbp 
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0 
    jge .pos 
    push rdi 
    mov rdi, '-' 
    call print_char
    pop rdi 
    neg rdi 
    .pos: 
      call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r13 
    push r14
    push rcx

    xor rcx, rcx
    xor r13, r13
    xor r14, r14
    .loop:
      mov r13b, byte[rsi + rcx] 
      mov r14b, byte[rdi + rcx] 
      cmp r13b, r14b 
      jne .err 
      cmp r14b, 0 
      je .finish
      inc rcx 
      jmp .loop
    .finish:
      mov rax, 1
      jmp .r
    .err:
      mov rax, 0
    .r:
      pop rcx
      pop r14
      pop r13
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp 
    mov rdx, 1 
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13 
    push r12

    xor r12, r12 
    xor r13, r13
    mov r13, rdi 
    .first:
      call read_char 
      cmp rax, 0     
      je .done       
      cmp rax, 0x9
      je .first
      cmp rax, 0x20
      je .first
      cmp rax, 0xA
      je .first

    .read: 
      cmp rsi, r12 
      je .overflow
      mov [r13 + r12], rax 
      inc r12 
      call read_char 
      cmp rax, 0 
      je .done   
      cmp rax, 0x9
      je .done
      cmp rax, 0x20
      je .done
      cmp rax, 0xA
      je .done

      jmp .read

    .done:
      xor rax, rax
      mov [r13 + r12], rax 
      mov rax, r13 
      mov rdx, r12 
      jmp .finish
    .overflow:
      xor rax, rax 
    .finish:
      pop r12 
      pop r13
      ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r13

    xor r13, r13
    xor rdx, rdx 
    xor rax, rax 

    .uint:
      cmp byte[rdi + rdx], '0' 
      jl .r
      cmp byte[rdi + rdx], '9' 
      jg .r

      mov r13b, byte[rdi + rdx] 
      sub r13b, '0' 
      imul rax, 10 
      add rax, r13 
      inc rdx 

      jmp .uint

    .r:
      pop r13
      ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось

parse_int:
    cmp byte[rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
    .positive:
      call parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; argumets - rdi, rsi, rdx
string_copy:
  push rbx 
  xor rax, rax 
  .loop:
      cmp rax, rdx 
      je .fail     
      mov rbx, [rdi + rax] 
      mov [rsi + rax], rbx 
      cmp rbx, 0 
      je .exit
      inc rax 
      jmp .loop
  .fail:
      xor rax, rax
      jmp .exit
  .exit:
      pop rbx 
      ret
